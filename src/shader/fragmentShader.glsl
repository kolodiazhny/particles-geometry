varying vec3 vPosition;
uniform vec3 uColor1;
uniform vec3 uColor2;
uniform float uTime;
uniform float rand1;

float getDistance(vec2 center, vec2 position) {
    float a = center.x - position.x;
    float b = center.y - position.y;
    return sqrt(a * a + b * b);
}

float getMod(float a, float b) {
    float result = a - (b * floor(a / b));
    return result;
}

float getColor(float value) {
    return value * uTime * 2.7 + 0.5;
}

void main() {
    //option 1
    // vec3 c1 = vec3(getColor(vPosition.y), getColor(vPosition.x), getColor(vPosition.z));
    // vec3 c2 = vec3(getColor(vPosition.z), getColor(vPosition.x), getColor(vPosition.y));
    // vec3 color = mix(c1, c2, fract(vPosition.x * 0.5 + sin(uTime * 1.5)));

    //option 2
    // vec3 color = mix(uColor1, uColor2, vPosition.x * 0.5 + 0.5);

    //option 3
    vec3 color = mix(fract(uColor1 * uTime * .5), sin(uColor2 * uTime * 2.5), vPosition.x * vPosition.y);
    
    if( getDistance( vec2( 0.5, 0.5 ), gl_PointCoord ) > 0.5 ){
        discard;
    } else {
        float alphaFactor = 1. - (getDistance( vec2( 0.5, 0.5 ), gl_PointCoord) * 2.5);

        // float f1 = 2.2;
        // float f2 = fract( uTime ) * 0.1 + 0.7;
        // gl_FragColor = vec4( vec3( 
        //     mix(uColor1.r, uColor2.r, getMod( vPosition.x + uTime, f1 * f2 ) / ( f1 * f2 ) ),
        //     mix(uColor1.g, uColor2.g, getMod( vPosition.y + uTime, f1 * f2 ) / ( f1 * f2 ) ),
        //     mix(uColor1.b, uColor2.b, getMod( vPosition.z + uTime, f1 * f2 ) / ( f1 * f2 ) )
        // ), alphaFactor );

        // gl_FragColor = vec4(fract(uColor1 * uTime * 100.), alphaFactor );
        gl_FragColor = vec4(color, alphaFactor);
    }
    

}