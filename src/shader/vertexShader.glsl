attribute vec3 aRandom;
varying vec3 vPosition;
uniform float uTime;
uniform float uScale;

void main() {
    vPosition = position;

    vec3 pos = position;

    float speedFactor = 5.;
    float distFactor = 0.005;
    float sizeFactor = 12.;
    float time = uTime * speedFactor;

    pos.x += cos(time * aRandom.x) * distFactor;
    pos.y += sin(time * aRandom.y) * distFactor;
    pos.z += sin(time * aRandom.z) * distFactor;

    pos.x *= uScale + (tan(pos.y * 4. + time) * (1. - uScale));
    pos.y *= uScale + (tan(pos.z * 4. + time) * (1. - uScale));
    pos.z *= uScale + (tan(pos.x * 4. + time) * (1. - uScale));

    pos *= uScale;

    vec4 mvPosition = modelViewMatrix * vec4( pos, 0.8 );
    gl_Position = projectionMatrix * mvPosition;
    gl_PointSize = sizeFactor / -mvPosition.z;
}